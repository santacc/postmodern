import fullpage from 'fullpage.js';
export default {
  init() {
    // JavaScript to be fired on the home page

    initialize(false);

    function initialize(hasScrollBar){
      new fullpage('#fullpage', {
        anchors:['inicio', 'the_origin','the_film', 'why_this', 'carl_sagan', 'disclaimers_and_FAQs','credits'],
        menu: '#menu',
        slidesNavigation: true,
        navigation: true,
        navigationPosition: 'right',
        navigationTooltips: ['Home', 'The Pioneer Plaque', 'The Short Film','Why this?', 'Carl Sagan', 'Disclaimers and FAQs', 'Credits'],
        showActiveTooltip: true,
        slidesNavPosition: 'right',
        parallax: true,
        parallaxKey: 'YWx2YXJvdHJpZ28uY29tXzlNZGNHRnlZV3hzWVhnPTFyRQ==',
        parallaxOptions: {
          type: 'reveal',
          percentage: 35,
          property: 'translate',
        },
        scrollingSpeed: 1500,
        autoScrolling: false,
        scrollBar: hasScrollBar,
        fitToSection:false,
        sectionsColor : [
          '#000',
          '#faf8ea',
          '#000',
          '#faf8ea',
          '#000',
          '#000',
          '#000',
          '#000',
          '#000',
        ],
      });
    }

/*



    var fullPageInstance = new fullpage('#fullpage', {
     //Navigation
     menu: '#menu',
     lockAnchors: true,
     anchors:['primeraPagina', 'segundaPagina','terceraPagina', 'cuartaPagina', 'quintaPagina', 'sextaPagina', 'septimaPagina','octavaPagina','novenaPagina'],
     navigation: true,
     navigationPosition: 'right',
     navigationTooltips: ['The Postmodern Pioneer Plaque', 'The orignial Pioneer Plaque', 'Our proyect','Why this?', 'This project is also', 'The Response', 'Credits', 'Disclaimers and FAQs', 'Contact'],
     showActiveTooltip: true,
     slidesNavigation: false,
     slidesNavPosition: 'right',

     //Scrolling
     css3: true,
     scrollingSpeed: 700,
     autoScrolling: true,
     fitToSection: true,
     fitToSectionDelay: 1000,
     scrollBar: false,
     easing: 'easeInOutCubic',
     easingcss3: 'ease',
     loopBottom: false,
     loopTop: false,
      loopHorizontal: true,
     continuousVertical: false,
     continuousHorizontal: false,
     scrollHorizontally: false,
     interlockedSlides: false,
     dragAndMove: false,
     offsetSections: false,
     resetSliders: false,
     fadingEffect: false,
     normalScrollElements: '#element1, .element2',
     scrollOverflow: false,
     scrollOverflowReset: false,
     scrollOverflowOptions: null,
     touchSensitivity: 15,
     bigSectionsDestination: null,

     //Accessibility
     keyboardScrolling: true,
     animateAnchor: true,
     recordHistory: true,

     //Design
     controlArrows: true,
    // verticalCentered: true,

     sectionsColor : [
       '#000',
       '#000',
       '#000',
       '#3e3c3c',
       '#000',
       '#000',
       '#000',
       '#000',
       '#000',
     ],
      paddingTop: '3em',
      paddingBottom: '10px',
      fixedElements: '#header, .footer',
      responsiveWidth: 0,
      responsiveHeight: 0,
      responsiveSlides: false,
      parallax: false,
      parallaxOptions: {
        type: 'reveal',
        percentage: 62,
        property: 'translate',
      },
      dropEffect: true,
      dropEffectOptions: {
        speed: 2300,
        color: '#F82F4D',
        zIndex: 9999,
      },
      waterEffect: true,
      waterEffectOptions: {
        animateContent: true,
        animateOnMouseMove: true,
      },
      cards: true,
      cardsOptions: {
        perspective: 100,
        fadeContent: true,
        fadeBackground: true,
      },

     //Custom selectors
     sectionSelector: '.section',
     slideSelector: '.section',

     lazyLoading: true,


     //events
    });
    console.log(fullPageInstance);
*/
//methods
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
