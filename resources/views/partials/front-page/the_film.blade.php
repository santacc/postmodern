<?php
  $fondoTheFilm = get_field('fondoTheFilm','options');
  $colorTheFilm = get_field('colorTheFilm','options');
  $titTheFilm = get_field('titTheFilm','options');
  $urlVideo = get_field('urlVideo','options');
?>
<div id="section2" class="section" style="background-image: url('<?php echo $fondoTheFilm["url"]; ?>'); background-size: cover; background-position: center">
<div class="container">
  <div class="row justify-content-center">
    <div class="col-12 col-md-8 text-center my-5">
      <h2><?php echo $titTheFilm; ?></h2>
    </div>
  </div>
  <div class="row justify-content-center">
    <div class="col-12 col-md-8">
      <div class="ratio ratio-16x9">
        <iframe class="embed-responsive-item" src="<?php echo $urlVideo; ?>" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>
</div>
