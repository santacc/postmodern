<?php
  $fondoWhyThis = get_field('fondoWhyThis', 'options');
  $colorWhyThis = get_field('colorWhyThis', 'options');
  $titWhyThis = get_field('titWhyThis', 'options');
  $txtWhyThis = get_field('txtWhyThis', 'options');
  $titWhyPostModern = get_field('titWhyPostModern', 'options');
  $txtWhyPostModern = get_field('txtWhyPostModern', 'options');
?>
<div id="section3" class="section" style="background-color: #faf8ea; color: #000">
<div class="container">
  <div class="row justify-content-center">
    <div class="col-12 col-md-8 py-5" style="line-height: 1.3rem">
      <h2 class="text-center p-5"><?php echo $titWhyThis; ?></h2>
      <?php echo $txtWhyThis; ?>

      <?php echo $txtWhyPostModern; ?>
    </div>
  </div>
</div>
</div>
