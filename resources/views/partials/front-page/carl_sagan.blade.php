<?php
  $fondoCarlSagan = get_field('fondoCarlSagan', 'options');
  $colorCarlSagan = get_field('colorCarlSagan', 'options');
  $txtUnoCarlSagan = get_field('txtUnoCarlSagan', 'options');
  $titCarlSagan = get_field('titCarlSagan', 'options');
  $txtCarlSagan = get_field('txtCarlSagan', 'options');

?>
<div id="section4" class="section" style="background-image: url('<?php echo $fondoCarlSagan["url"];?>'); background-size: cover; background-position: center">
  <div class="container">
    <div class="row justify-content-center align-items-baseline">
      <div class="col-12 col-md-8" style="padding-top: 30%">
        <p><?php echo $txtUnoCarlSagan; ?></p>
        <h2><?php echo $titCarlSagan; ?></h2>
        <?php echo $txtCarlSagan; ?>
      </div>
    </div>
  </div>
</div>
