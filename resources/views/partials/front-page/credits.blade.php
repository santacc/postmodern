<?php

 $fondoCredits = get_field('fondoCredits','options');
   $colorCredits = get_field('colorCredits','options');
   $titCreditos = get_field('titCreditos','options');
   $imgBoris = get_field('imgBoris','options');
   $txtIzqCreditos = get_field('txtIzqCreditos','options');
   $txtUnoCreditos = get_field('txtUnoCreditos','options');
   $txtDosACreditos = get_field('txtDosACreditos','options');
   $txtDosBCreditos = get_field('txtDosBCreditos','options');
   $txtDosCCreditos = get_field('txtDosCCreditos','options');
   $txtTresCreditos = get_field('txtTresCreditos','options');
   $txtCuatroCreditos = get_field('txtCuatroCreditos','options');
  ?>
<div id="section6" class="section" style="background-color: <?php echo $colorCredits; ?> !important">
  <div class="container py-5">
    <div class="row justify-content-center">
    <div class="col-6 col-md-2">
      <img src="<?php echo $imgBoris["url"]; ?>" width="100%">

    </div>
    </div>
    <div class="row justify-content-center mt-2 mb-5">
       <div class="col-10 col-md-5 text-center">
        <?php echo $txtIzqCreditos; ?>
      </div>
    </div>
      <div class="row justify-content-center">
    <div class="col-12 col-md-8 text-center">

      <?php echo $txtUnoCreditos; ?>
      <div class="row py-3">
        <div class="col-4"><?php echo $txtDosACreditos; ?></div>
        <div class="col-4"><?php echo $txtDosBCreditos; ?></div>
        <div class="col-4"><?php echo $txtDosCCreditos; ?></div>
      </div>
      <div class="row justify-content-center py-3">
        <div class="col-6">
          <?php echo $txtTresCreditos; ?>
        </div>
      </div>
      <div class="row justify-content-center  py-3">
        <div class="col-12">
         <?php echo $txtCuatroCreditos; ?>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>
