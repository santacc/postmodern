<?php
    $fondoOrigin = get_field('fondoOrigin','options');
   $colorOrigin = get_field('colorOrigin','options');
   $textoIntroOrigin = get_field('textoIntroOrigin','options');

   $imagenCentroOrigin = get_field('imagenCentroOrigin','options');
   $imagenInfOrigin = get_field('imagenInfOrigin','options');

?>
<div id="section1" class="section" style="background-color: #faf8ea; color: #000">
<div class="container py-5">
  <div class="row justify-content-center">
    <div class="col-12 col-md-5 pe-5" style="line-height: 2rem !important">
      <?php echo $textoIntroOrigin; ?>

    </div>
    <div class="col-12 col-md-3">
      <img src="<?php echo $imagenCentroOrigin["url"]; ?>" width="100%" style="margin-bottom: 10px"><br />
      <img src="<?php echo $imagenInfOrigin["url"]; ?>" width="100%">
    </div>


  </div>
</div>
</div>

<style>
  p {
    line-height: 2rem;
  }
</style>
