<?php

  $fondoHeader = get_field('fondoHeader','options');
  $colorHeader = get_field('colorHeader','options');
  $logoHeader = get_field('logoHeader','options');
?>
<div id="section0" class="section" style="background-image: url('<?php echo $fondoHeader["url"]; ?>'); background-size: cover; background-position: top center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-12 col-md-6 text-center">
        <img src="<?php echo $logoHeader["url"]; ?>" width="100%">

      </div>
    </div>
  </div>
</div>
