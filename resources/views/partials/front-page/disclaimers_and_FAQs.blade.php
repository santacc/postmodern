<?php
$fondoFaqs = get_field('fondoFaqs','options');
  $colorFaqs = get_field('colorFaqs','options');
  $iconoUno = get_field('iconoUno','options');
  $txtUno = get_field('txtUno','options');
  $iconoDos = get_field('iconoDos','options');
  $txtDos = get_field('txtDos','options');
  $iconoTres = get_field('iconoTres','options');
  $txTres = get_field('txTres','options');
?>
<div id="section7" class="section" style="background-image: url('<?php echo $fondoFaqs["url"]; ?>'); background-size: cover; background-position: center">
  <div class="container py-5 my-4">
    <div class="row justify-content-center my-2">
      <div class="col-6 col-md-2 text-center "> <img src="<?php echo $iconoUno["url"]; ?>" width="50%"></div>
      <div class="col-12 col-md-6">
        <?php echo $txtUno; ?>
      </div>
    </div>
    <div class="row justify-content-center my-2">
      <div class="col-6 col-md-2 text-center"><img src="<?php echo $iconoDos["url"]; ?>" width="50%"></div>
      <div class="col-12 col-md-6">
       <?php echo $txtDos; ?>
      </div>
    </div>
    <div class="row justify-content-center my-2">
      <div class="col-6 col-md-2 text-center"><img src="<?php echo $iconoTres["url"]; ?>" width="50%"></div>
      <div class="col-12 col-md-6">
        <?php echo $txTres; ?>
      </div>
    </div>
  </div>
</div>
