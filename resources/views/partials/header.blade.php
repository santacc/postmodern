


<header class="banner">
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <ul id="menu" class="navbar-nav me-auto mb-2 mb-lg-0">
        <li data-menuanchor="inicio" class=""><a href="#inicio">Home</a></li>
        <li data-menuanchor="the_origin" class=""><a href="#the_origin">The Pioneer Plaque</a></li>
        <li data-menuanchor="the_film" class="active"><a href="#the_film">The Short Film</a></li>
        <li data-menuanchor="why_this" class=""><a href="#why_this">Why this?</a></li>
       <li data-menuanchor="carl_sagan" class=""><a href="#carl_sagan">Carl Sagan</a></li>
       <!-- <li data-menuanchor="the_response" class=""><a href="#the_response">The Response</a></li> -->
        <li data-menuanchor="disclaimers_and_FAQs" class=""><a href="#disclaimers_and_FAQs">Disclaimers and FAQs</a></li>
        <li data-menuanchor="credits" class=""><a href="#credits">Credits</a></li>
      </ul>
    </nav>
  </div>
</header>



