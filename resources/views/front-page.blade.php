@extends('layouts.app')

@section('content')

  <div id="fullpage">
    <!-- Header -->
    @include('partials.front-page.start')
    <!-- The Orignial Pioneer Plaque -->
    @include('partials.front-page.the_orignial_pioneer_plaque')
    <!-- the_film -->
    @include('partials.front-page.the_film')
    <!-- Why This? -->
    @include('partials.front-page.why_this')
    <!-- Carl Sagan -->
     @include('partials.front-page.carl_sagan')

  <!-- The Response -->
  <!-- <div id="section5" class="section" style="background-image: url('/wp-content/themes/postmodern/resources/assets/images/fondo_estrellas.jpg'); background-size: cover; background-position: center">
      {{--@include('partials.front-page.the_response') --}}
    </div> -->
    <!-- Disclaimers and FAQs -->
    @include('partials.front-page.disclaimers_and_FAQs')
    <!-- Credits -->
    @include('partials.front-page.credits')
</div>

@endsection
