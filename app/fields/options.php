<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

acf_add_options_page([
    'page_title' => get_bloginfo('name') . ' theme options',
    'menu_title' => 'PostModern Options',
    'menu_slug'  => 'theme-options',
    'capability' => 'edit_theme_options',
    'position'   => '999',
    'autoload'   => true
]);

$options = new FieldsBuilder('theme_options');

$options
    ->setLocation('options_page', '==', 'theme-options');

$options
    ->addTab('Home', ['placement' => 'left'])
        ->addImage('fondoHeader', [
            'label' => 'Imagen Fondo Seccion',
        ])
        ->addColorPicker('colorHeader', [
            'label' => 'Color Fondo',
        ])
        ->addImage('logoHeader', [
            'label' => 'Logo para el header',
        ])
    ->addTab('The Origin', ['placement' => 'left'])
        ->addImage('fondoOrigin', [
            'label' => 'Imagen Fondo Seccion',
        ])
        ->addColorPicker('colorOrigin', [
        'label' => 'Color Fondo',
        ])
        ->addWysiwyg('textoIntroOrigin', [
        'label' => 'Texto intro Origin',
        ])
        ->addImage('imagenCentroOrigin', [
            'label' => 'Imagen Uno derecha',
        ])
        ->addImage('imagenInfOrigin', [
            'label' => 'Imagen Dos derecha',
        ])


    ->addTab('THE FILM', ['placement' => 'left'])
        ->addImage('fondoTheFilm', [
            'label' => 'Imagen Fondo Seccion',
        ])
        ->addColorPicker('colorTheFilm', [
            'label' => 'Color Fondo',
        ])
        ->addWysiwyg('titTheFilm', [
            'label' => 'Titulo the film',
        ])
        ->addText('urlVideo', [
        'label' => 'URL para el video',
    ])

    ->addTab('Why this?', ['placement' => 'left'])
        ->addImage('fondoWhyThis', [
            'label' => 'Imagen Fondo Seccion',
        ])
        ->addColorPicker('colorWhyThis', [
            'label' => 'Color Fondo',
        ])
        ->addText('titWhyThis', [
            'label' => 'Titulo columna why This',
        ])
        ->addWysiwyg('txtWhyThis', [
        'label' => 'Texto Why This',
        ])
        ->addText('titWhyPostModern', [
            'label' => 'Titulo columna why postmodern',
        ])
        ->addWysiwyg('txtWhyPostModern', [
        'label' => 'Texto Why This',
        ])

    ->addTab('Carl Sagan', ['placement' => 'left'])
        ->addImage('fondoCarlSagan', [
            'label' => 'Imagen Fondo Seccion',
        ])
        ->addColorPicker('colorCarlSagan', [
            'label' => 'Color Fondo',
        ])
        ->addText('txtUnoCarlSagan', [
            'label' => 'Titulo columna why postmodern',
        ])
        ->addText('titCarlSagan', [
            'label' => 'Titulo columna why postmodern',
        ])
        ->addWysiwyg('txtCarlSagan', [
        'label' => 'Texto Why This',
    ])

    ->addTab('The Response', ['placement' => 'left'])
        ->addImage('fondoTheResponse', [
            'label' => 'Imagen Fondo Seccion',
        ])
        ->addColorPicker('colorTheResponse', [
        'label' => 'Color Fondo',
        ])

    ->addTab('Disclaimers and FAQs', ['placement' => 'left'])
        ->addImage('fondoFaqs', [
            'label' => 'Imagen Fondo Seccion',
        ])
        ->addColorPicker('colorFaqs', [
        'label' => 'Color Fondo',
        ])
        ->addImage('iconoUno', [
            'label' => 'Icono Del primer punto',
        ])
        ->addWysiwyg('txtUno', [
        'label' => 'Texto de la seccion uno',
    ])
        ->addImage('iconoDos', [
            'label' => 'Icono del segundo punto',
        ])
        ->addWysiwyg('txtDos', [
            'label' => 'Texto de la seccion dos',
        ])
        ->addImage('iconoTres', [
            'label' => 'Icono Del tercer  punto',
        ])
        ->addWysiwyg('txTres', [
        'label' => 'Texto de la seccion tres',
        ])

    ->addTab('Credits', ['placement' => 'left'])
        ->addImage('fondoCredits', [
            'label' => 'Imagen Fondo Seccion',
        ])
        ->addColorPicker('colorCredits', [
        'label' => 'Color Fondo',
        ])
        ->addText('titCreditos', [
        'label' => 'Titulo De seccion',
        ])
        ->addImage('imgBoris', [
            'label' => 'Imagen Seccion Izquierda',
        ])
        ->addWysiwyg('txtIzqCreditos', [
            'label' => 'Texto Seccion Izquierda',
        ])
        ->addWysiwyg('txtUnoCreditos', [
        'label' => 'Texto primer grupo',
        ])
        ->addWysiwyg('txtDosACreditos', [
            'label' => 'Texto segundo grupo col 1',
        ])
        ->addWysiwyg('txtDosBCreditos', [
            'label' => 'Texto segundo grupo col 2',
        ])
        ->addWysiwyg('txtDosCCreditos', [
            'label' => 'Texto segundo grupo col 3',
        ])
        ->addWysiwyg('txtTresCreditos', [
            'label' => 'Texto tercer grupo',
        ])
        ->addWysiwyg('txtCuatroCreditos', [
            'label' => 'Texto cuarto grupo',
        ])
    ;



return $options;
